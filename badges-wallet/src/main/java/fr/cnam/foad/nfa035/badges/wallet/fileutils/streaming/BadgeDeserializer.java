package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Interface
 *
 * @param <M>
 */
public interface BadgeDeserializer<M extends ImageFrameMedia> {

    /**
     * Méthode e désérialisation,
     * @param media
     * @throws IOException
     */
    void deserialize(M media) throws IOException;

    /**
     * récupérer un Flux
     *
     * @param <T>
     * @return
     */
    <T extends OutputStream> T getSourceOutputStream();

    /**
     * modifier le flux
     *
     * @param os
     * @param <T>
     */
    <T extends OutputStream> void setSourceOutputStream(T os);


}
