package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.BufferedReader;
import java.io.IOException;

/**
 *
 * @param <T>
 */
public interface ResumableImageFrameMedia<T> extends ImageFrameMedia<T> {
    /**
     *
     * @param resume
     * @return
     * @throws IOException
     */
    BufferedReader getEncodedImageReader(boolean resume) throws IOException;
}
